import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BollingerComponent } from './bollinger/bollinger.component';
import { CreateTradeComponent } from './create-trade/create-trade.component';
import { HomeComponent } from './home/home.component';
import { IndividualTradeComponent } from './individual-trade/individual-trade.component';
import { TradeComponent } from './trade/trade.component';
import { BondComponent } from './bond/bond.component';
import { CommodityComponent } from './commodity/commodity.component';
import { EquityComponent } from './equity/equity.component';
import { CreateEquityComponent } from './create-equity/create-equity.component';
import { CreateCommodityComponent } from './create-commodity/create-commodity.component';


const routes: Routes = [
  {path: '', component:HomeComponent},
  {path: 'trades', component:TradeComponent},
  {path: 'individual/:id', component:IndividualTradeComponent},
  {path: 'bonds', component:BondComponent},
  {path: 'commodities', component:CommodityComponent},
  {path: 'equities', component:EquityComponent},
  {path: 'bollinger', component:BollingerComponent},
  {path: 'bollinger/:id', component:BollingerComponent},
  {path: 'createtrade', component:CreateTradeComponent},
  {path: 'createequity', component:CreateEquityComponent},
  {path: 'createcommodity', component:CreateCommodityComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
