import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TradeComponent } from './trade/trade.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PieChartComponent } from './pie-chart/pie-chart.component';
import { IndividualTradeComponent } from './individual-trade/individual-trade.component';
import { HomeComponent } from './home/home.component';
import { BondComponent } from './bond/bond.component';
import { CommodityComponent } from './commodity/commodity.component';
import { EquityComponent } from './equity/equity.component'
import { BollingerComponent } from './bollinger/bollinger.component';
import { CreateTradeComponent } from './create-trade/create-trade.component';
import { CreateEquityComponent } from './create-equity/create-equity.component';
import { CreateCommodityComponent } from './create-commodity/create-commodity.component'

@NgModule({
  declarations: [
    AppComponent,
    TradeComponent,
    PieChartComponent,
    IndividualTradeComponent,
    HomeComponent,
    BondComponent,
    CommodityComponent,
    EquityComponent,
    BollingerComponent,
    CreateTradeComponent,
    CreateEquityComponent,
    CreateCommodityComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
