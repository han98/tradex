import { Component, OnInit } from '@angular/core';
import { BandService } from 'src/services/bands.service';

@Component({
  selector: 'app-bollinger',
  templateUrl: './bollinger.component.html',
  styleUrls: ['./bollinger.component.css']
})
export class BollingerComponent implements OnInit {

  //models
  data
  showBox = false
  ticker:string

  constructor(private bandService: BandService) { }

  getAdvice(){
    this.ticker = (document.getElementById("search") as HTMLInputElement).value;
    this.showBox = true
    this.bandService.getBollingerBand(this.ticker)
      .subscribe( (repsonse) =>{this.data = repsonse})
  }

  ngOnInit(): void {
  }

}
