import { Component, OnInit } from '@angular/core';
import { BondService } from 'src/services/bond.service';

@Component({
  selector: 'app-bond',
  templateUrl: './bond.component.html',
  styleUrls: ['./bond.component.css']
})
export class BondComponent implements OnInit {

  data

  constructor(private bondService:BondService) { }

  getAllBonds(){
    this.bondService.getBonds()
    .subscribe( (response)=>{this.data = response})
  }

  getBondById(id){
    this.bondService.getBond(id)
    .subscribe( (response)=>{this.data = response} )
  }

  ngOnInit(): void {
    this.getAllBonds()
    
  }

}
