import { Component, OnInit } from '@angular/core';
import { CommodityService } from 'src/services/commodity.service';

@Component({
  selector: 'app-commodity',
  templateUrl: './commodity.component.html',
  styleUrls: ['./commodity.component.css']
})
export class CommodityComponent implements OnInit {

  data

  constructor(private commodityService:CommodityService) { }

  getAllCommodities(){
    this.commodityService.getCommodities()
    .subscribe( (response)=>{this.data = response})
  }

  getCommodityById(id){
    this.commodityService.getCommodity(id)
    .subscribe( (response)=>{this.data = response})
  }

  ngOnInit(): void {
    this.getAllCommodities()

  }

}
