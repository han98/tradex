import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { CommodityService } from 'src/services/commodity.service';

@Component({
  selector: 'app-create-commodity',
  templateUrl: './create-commodity.component.html',
  styleUrls: ['./create-commodity.component.css']
})
export class CreateCommodityComponent implements OnInit {

  createCommodityForm;

  constructor(private formBuilder: FormBuilder, private commodityService: CommodityService) { }

  isInvalid(control) {
    return this.createCommodityForm.controls[control].invalid &&
      this.createCommodityForm.controls[control].touched;
  }

  isUntouched() {
    return this.createCommodityForm.controls.pristine;
  }

  isIncomplete() {
    return this.isInvalid('ticker') ||
      this.isInvalid('quantity') ||
      this.isInvalid('price') ||
      this.isInvalid('state')
  }

  onSubmit() {
    let myVar = this.createCommodityForm.postNewEquity(this.createCommodityForm.value)
    myVar.subscribe((data) => { console.log(data) }
    );
    this.createCommodityForm.reset();
  }

  ngOnInit(): void {
    let date: Date = new Date();  
    this.createCommodityForm = this.formBuilder.group({
      dateCreated: [date],
      ticker: ['', Validators.required],
      quantity: [],
      price: ['', Validators.required],
      state: []
    })
  }

}
