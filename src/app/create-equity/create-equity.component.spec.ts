import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEquityComponent } from './create-equity.component';

describe('CreateEquityComponent', () => {
  let component: CreateEquityComponent;
  let fixture: ComponentFixture<CreateEquityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEquityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEquityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
