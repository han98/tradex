import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { EquityService } from 'src/services/equity.service';
import { TraderService } from 'src/services/trader.service';

@Component({
  selector: 'app-create-equity',
  templateUrl: './create-equity.component.html',
  styleUrls: ['./create-equity.component.css']
})
export class CreateEquityComponent implements OnInit {

  createEquityForm;

  constructor(private formBuilder: FormBuilder, private equityService: EquityService) { }

  isInvalid(control) {
    return this.createEquityForm.controls[control].invalid &&
      this.createEquityForm.controls[control].touched;
  }

  isUntouched() {
    return this.createEquityForm.controls.pristine;
  }

  isIncomplete() {
    return this.isInvalid('ticker') ||
      this.isInvalid('quantity') ||
      this.isInvalid('amount') ||
      this.isInvalid('equityStatus')
  }

  onSubmit() {
    let myVar = this.equityService.postNewEquity(this.createEquityForm.value)
    myVar.subscribe((data) => { console.log(data) }
    );
    this.createEquityForm.reset();
  }

  ngOnInit(): void {
    let date: Date = new Date();  
    this.createEquityForm = this.formBuilder.group({
      dateCreated: [date],
      ticker: ['', Validators.required],
      quantity: [],
      amount: ['', Validators.required],
      equityStatus: []
    })
  }

}
