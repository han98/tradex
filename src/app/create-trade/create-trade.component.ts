import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { TraderService } from 'src/services/trader.service';

@Component({
  selector: 'app-create-trade',
  templateUrl: './create-trade.component.html',
  styleUrls: ['./create-trade.component.css']
})
export class CreateTradeComponent implements OnInit {

  createTradeForm;

  constructor(private formBuilder: FormBuilder, private tradeService: TraderService) { }

  isInvalid(control) {
    return this.createTradeForm.controls[control].invalid &&
      this.createTradeForm.controls[control].touched;
  }

  isUntouched() {
    return this.createTradeForm.controls.pristine;
  }

  isIncomplete() {
    return this.isInvalid('ticker') ||
      this.isInvalid('quantity') ||
      this.isInvalid('amount') ||
      this.isInvalid('tradeType')
  }

  onSubmit() {
    let myVar = this.tradeService.postNewTrade(this.createTradeForm.value)
    myVar.subscribe((data) => { console.log(data) }
    );
    this.createTradeForm.reset();
  }

  ngOnInit(): void {
    let date: Date = new Date();  
    this.createTradeForm = this.formBuilder.group({
      dateCreated: [date],
      ticker: ['', Validators.required],
      quantity: [],
      amount: ['', Validators.required],
      tradeType: []
    })
  }
}
