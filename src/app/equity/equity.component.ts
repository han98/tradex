import { Component, OnInit } from '@angular/core';
import { EquityService } from 'src/services/equity.service';

@Component({
  selector: 'app-equity',
  templateUrl: './equity.component.html',
  styleUrls: ['./equity.component.css']
})
export class EquityComponent implements OnInit {

  data

  constructor(private equityService:EquityService) { }

  getAllEquities(){
    this.equityService.getEquities()
    .subscribe( (response)=>{this.data = response})
  }

  ngOnInit(): void {
    this.getAllEquities()
  }

}
