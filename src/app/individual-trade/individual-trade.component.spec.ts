import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualTradeComponent } from './individual-trade.component';

describe('IndividualTradeComponent', () => {
  let component: IndividualTradeComponent;
  let fixture: ComponentFixture<IndividualTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
