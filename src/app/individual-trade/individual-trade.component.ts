import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TraderService } from 'src/services/trader.service';

@Component({
  selector: 'app-individual-trade',
  templateUrl: './individual-trade.component.html',
  styleUrls: ['./individual-trade.component.css']
})
export class IndividualTradeComponent implements OnInit {

  data

  constructor(private tradeService:TraderService,
     private route:ActivatedRoute) { }

  // button handler method
  getOneTrade(id){
    // console.log('get from code')
    this.tradeService.getTrade(id)
    .subscribe( (response)=>{this.data = response} )
  }

  ngOnInit(): void {
    // when this component loads, run this call to an Observable method
    this.getOneTrade(this.route.snapshot.params.id)
  }

}
