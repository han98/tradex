import { Component, OnInit } from '@angular/core';
import { TraderService } from 'src/services/trader.service';

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.css']
})
export class TradeComponent implements OnInit {

  data

  constructor(private tradeService:TraderService) { }

  // button handler method
  getAllTrades(){
    // console.log('get from code')
    this.tradeService.getTrades()
    .subscribe( (response)=>{this.data = response} )
  }

  ngOnInit(): void {
    // when this component loads, run this call to an Observable method
    this.getAllTrades()
  }

}
