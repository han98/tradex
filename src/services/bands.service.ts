import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BandService {
  url = 'http://localhost:5000/'

  constructor(private http: HttpClient) { }

  // Get One Band
  getBollingerBand(ticker) {
    let fullUrl = `${this.url}${ticker}`
    return this.http.get(fullUrl)
  }
}