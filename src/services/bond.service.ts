import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BondService {
  url = 'http://citieurlinux6.conygre.com:8085/api/portfolio/bond/'

  constructor(private http: HttpClient) { }

  // Get All Bondss
  getBonds() {
    return this.http.get(this.url)
    
  }

  // Get One Bond
  getBond(id) {
    let fullUrl = `${this.url}${id}`
    return this.http.get(fullUrl)

  }

  //Post New Bond
  postNewBond(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    }
    return this.http.post(this.url, data, httpOptions)
  }

  //Update One Bond
  putUpdateBond(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.put(this.url, data, httpOptions)
  }
}