import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommodityService {
  url = 'http://citieurlinux6.conygre.com:8085/api/portfolio/commodity/'

  constructor(private http: HttpClient) { }

  // Get All Commodity
  getCommodities() {
    return this.http.get(this.url)
    
  }

  // Get One Commodity
  getCommodity(id) {
    let fullUrl = `${this.url}${id}`
    return this.http.get(fullUrl)

  }

  //Post New Commodity
  postNewCommodity(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    }
    return this.http.post(this.url, data, httpOptions)
  }

  //Update One Commodity
  putUpdateCommodity(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.put(this.url, data, httpOptions)
  }
}