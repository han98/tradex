import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EquityService {
  url = 'http://citieurlinux6.conygre.com:8085/api/portfolio/equity'

  constructor(private http: HttpClient) { }

  // Get All Equities
  getEquities() {
    return this.http.get(this.url)
    
  }

  // Get One Equity
  getEquity(id) {
    let fullUrl = `${this.url}${id}`
    return this.http.get(fullUrl)

  }

  //Post New Equity
  postNewEquity(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
      })
    }
    return this.http.post(this.url, data, httpOptions)
  }

  //Update One Equity
  putUpdateEquity(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.put(this.url, data, httpOptions)
  }
}