import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TraderService {
  url = 'http://citieurlinux6.conygre.com:8080/api/trader'

  constructor(private http: HttpClient) { }

  // Get All Trades
  getTrades() {
    return this.http.get(this.url)

  }

  // Get One Trade
  getTrade(id) {
    let fullUrl = `${this.url}${id}`
    return this.http.get(fullUrl)

  }

  //Post New Trade
  postNewTrade(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return this.http.post(this.url, data, httpOptions)
  }

  //Update One Trade
  putUpdateTrade(data) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }
    return this.http.put(this.url, data)
  }
}